# personal-laptop-provisioner

## Desciption

A personal ansible playbook to setup my system after a fresh install an keep it up to date.

For this project im using ansible modules like [apt](https://docs.ansible.com/ansible/latest/modules/apt_module.html), [systemd](https://docs.ansible.com/ansible/latest/modules/systemd_module.html?highlight=systemd), [url_get](https://docs.ansible.com/ansible/latest/modules/get_url_module.html?highlight=get_url), [unarchive](https://docs.ansible.com/ansible/latest/modules/unarchive_module.html?highlight=unarchive), [file](https://docs.ansible.com/ansible/latest/modules/file_module.html?highlight=file), [snap](https://docs.ansible.com/ansible/latest/modules/snap_module.html?highlight=snap) and [pip](https://docs.ansible.com/ansible/latest/modules/pip_module.html?highlight=pip).

This playbook should work fine on any debian based distribution.

## Requirements

- python3
- pip3 (`apt install python3-pip`)
- ansible 2.8+ (`pip3 install ansible`)

## Usage

To provision your laptop right after deployment run

```
ansible-playbook -K -i hosts install.yml 
```

To update your system on weekly basis run

```
ansible-playbook -K -i hosts update.yml 
```

## Tools avilable here

- playbook tools
  - music
	- spotify
  - video
	- obs
	- mplayer
  - editors
	- emacs
	- [mu-editor](https://codewith.mu/) [for the kids]
  - terminal tools
	- Tmux
	- Calendar
	- Aria2c
	- [ix](http://ix.io/)
  - devops tools
	- git
	- Docker ce
	- helm
	- Curl
	- Wget
  - imagen
	- jpegoptim
	- optipng
  - network
	- dnsutils
	- mtr
  - productivity
    - keepassxc
    - flameshot
    - easystroke
