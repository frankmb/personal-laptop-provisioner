#!/usr/bin/bash

install_pip() {
    sudo apt udpate -y
    sudo apt install -y python3-pip
}
check_pip() {
    which pip3 || install_pip
}

check_ansible() {
    check_pip
    which ansible-playbook || sudo -H pip3 install ansible
}

do_install() {
    check_ansible
    ansible-playbook -K -i hosts install.yml
}

do_update() {
    check_ansible
    ansible-playbook -K -i hosts update.yml
}

do_test() {
    check_ansible
    ansible-playbook -K -i hosts test.yml
}

print_help() {
    cat << EOF
Usage:

- `basename ${0}` [update|-u]
  to update all packages at once

- `basename ${0}` [install|-i]
  to install all packages at once

- `basename ${0}` [test|-t]
  to test the test role

- `basename ${0}` [help|-h]
  to see this message
EOF
}


case "$1" in
"install" | "-i")
    do_install
    ;;
"update" | "-u")
    do_update
    ;;
"test" | "-t")
    do_test
    ;;
*)
    print_help
    ;;
esac
